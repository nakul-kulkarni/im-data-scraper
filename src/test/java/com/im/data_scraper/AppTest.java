package com.im.data_scraper;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
	
    /**
     * Create the test case
     *
     * @param testName name of the test case
     * @throws IOException 
     * @throws org.json.simple.parser.ParseException 
     * @throws ParseException 
     * @throws org.json.simple.parser.ParseException 
     */
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public static void main(String[] args) throws IOException, org.json.simple.parser.ParseException{
		App app = new App();
		
		Document doc = org.jsoup.Jsoup.connect("http://facebook.com/39935792947")
				.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36")
				//.header("accept", "*/*")
				.header("accept-language", "en-US,en;q=0.9")
				//.header("accept-encoding", "gzip, deflate, br")
				.timeout(10 * 10000).get();
		// For pages:
		Element content = doc.getElementById("PagesLikesCountDOMID");
		if (null != content && content.child(0).childNodes().size() > 0) {
			String pageLikes = content.child(0).childNodes().get(0).toString();
			System.out.println("likes: ----"+pageLikes);
		}
		/*Document doc = org.jsoup.Jsoup.connect("https://www.instagram.com/p/BOwS5oejozR/").timeout(10*10000).get();
		Node node = doc.childNode(1).childNode(2).childNode(3).childNode(0);
		String data = node.attr("data").replaceAll("window._sharedData = ", "").replaceAll(";", "");
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(data);
		JSONObject entry = (JSONObject) jsonObject.get("entry_data");
		JSONArray jsonArray = (JSONArray) entry.get("PostPage");
		JSONObject profile = (JSONObject) jsonArray.get(0);
		JSONObject graphql = (JSONObject) profile.get("graphql");
		JSONObject mediaData = (JSONObject) graphql.get("shortcode_media");
		JSONObject commentEdge = (JSONObject) mediaData.get("edge_media_to_comment");
		JSONObject likeEdge = (JSONObject) mediaData.get("edge_media_preview_like");
		JSONObject titleEdge = (JSONObject) mediaData.get("edge_media_to_caption");

		JSONArray edgesArray = (JSONArray) titleEdge.get("edges");
		JSONObject nodeEdge = null;
		JSONObject nodeObject = null;
		if(edgesArray.size() > 0 ){
			nodeEdge = (JSONObject) edgesArray.get(0);
			nodeObject = (JSONObject) nodeEdge.get("node");
		}
		JSONObject owner = (JSONObject) mediaData.get("owner");
		
		
		JSONObject videoData = new JSONObject();
		videoData.put("videoId",mediaData.get("id"));
		if(null != nodeObject){
			videoData.put("title", nodeObject.get("text"));
		}else{
			videoData.put("title", null);
		}
		videoData.put("thumbnail", mediaData.get("display_url"));
		videoData.put("videoLink", mediaData.get("video_url"));
		videoData.put("isVideo", mediaData.get("is_video"));
		videoData.put("videoViewsCount", mediaData.get("video_view_count"));
		videoData.put("commentsCount", commentEdge.get("count"));
		videoData.put("likesCount", likeEdge.get("count"));
		videoData.put("accountId", owner.get("id"));
		videoData.put("accountName", owner.get("username"));
		videoData.put("publishedAt", mediaData.get("taken_at_timestamp"));
		
		System.out.println(videoData);*/
		
		
		/*Document doc = org.jsoup.Jsoup.connect("https://www.instagram.com/knorrph/").timeout(10 * 10000).get();
		Node node = doc.childNode(1).childNode(2).childNode(3).childNode(0);
		String data = node.attr("data").replaceAll("window._sharedData = ", "").replaceAll(";", "");
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(data);
		JSONObject entry = (JSONObject) jsonObject.get("entry_data");
		JSONArray jsonArray = (JSONArray) entry.get("ProfilePage");
		JSONObject profile = (JSONObject) jsonArray.get(0);
		JSONObject graphql = (JSONObject) profile.get("graphql");
		JSONObject user = (JSONObject) graphql.get("user");
		JSONObject mediaEdge = (JSONObject) user.get("edge_owner_to_timeline_media");
		JSONObject followEdge = (JSONObject) user.get("edge_follow");
		JSONObject followedByEdge = (JSONObject) user.get("edge_followed_by");
		
		JSONArray edges = (JSONArray) mediaEdge.get("edges");
		System.out.println("Size of media: "+ edges.size());
		JSONArray videoDataArr = new JSONArray();
		for(int i=0; i<edges.size();i++){
			JSONObject videoData = new JSONObject();
			JSONObject videoLevelData = (JSONObject) edges.get(0);
			JSONObject edgeNode = (JSONObject) videoLevelData.get("node");
			JSONObject likeEdge = (JSONObject) edgeNode.get("edge_liked_by");
			JSONObject titleEdge = (JSONObject) edgeNode.get("edge_media_to_caption");
			JSONArray edgesArray = (JSONArray) titleEdge.get("edges");
			JSONObject nodeEdge = (JSONObject) edgesArray.get(0);
			JSONObject nodeObject = (JSONObject) nodeEdge.get("node");
			
			videoData.put("mediaId", edgeNode.get("id"));
			videoData.put("isVideo", edgeNode.get("is_video"));
			videoData.put("videoURLName", edgeNode.get("shortcode"));
			videoData.put("likesCount", likeEdge.get("count"));
			videoData.put("title", nodeObject.get("text"));
			JSONObject videoComment = (JSONObject) edgeNode.get("edge_media_to_comment");
			videoData.put("commentsCount", videoComment.get("count"));
			videoData.put("publisehdAt", edgeNode.get("taken_at_timestamp"));
			videoData.put("thumbnail", edgeNode.get("thumbnail_src"));
			videoData.put("viewsCount", edgeNode.get("video_view_count"));
			
			videoDataArr.add(videoData);
		}
		
		 * String fullName = user.get("full_name").toString(); String
		 * userName = user.get("username").toString(); String id =
		 * user.get("id").toString(); String profilePicture =
		 * user.get("profile_pic_url").toString(); String followsCount =
		 * followEdge.get("count").toString(); String followedByCount =
		 * followedByEdge.get("count").toString(); String mediaCount =
		 * mediaEdge.get("count").toString();
		 
		JSONObject accountDetails = new JSONObject();
		accountDetails = new JSONObject();
		accountDetails.put("accountId", user.get("id"));
		accountDetails.put("userName", user.get("username"));
		accountDetails.put("fullName", user.get("full_name"));
		accountDetails.put("profilePicture", user.get("profile_pic_url"));
		accountDetails.put("mediaCount", mediaEdge.get("count"));
		accountDetails.put("followsCount", followEdge.get("count"));
		accountDetails.put("followedByCount", followedByEdge.get("count"));
		accountDetails.put("mediaDiscovered", videoDataArr);
		
		
		System.out.println(accountDetails.toJSONString());*/
		
		
		
		
		
		/*URL urlLink = new URL("https://www.instagram.com/knorrph/");
		HttpURLConnection httpURLConnection = (HttpURLConnection) urlLink.openConnection();
		//add request header
		httpURLConnection.setRequestProperty("User-Agent", Constants.USER_AGENT);

		int responseCode = httpURLConnection.getResponseCode();
		//System.out.println("\nSending 'GET' request to URL : " + videoURL);
		System.out.println("Response Code : " + responseCode);
		
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(httpURLConnection.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		PrintWriter printWriter = new PrintWriter(new FileWriter("/Users/Nakul/Documents/insta_output_account.txt"));
		printWriter.write(response.toString());
		printWriter.flush();
		printWriter.close();*/
	}
	
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
