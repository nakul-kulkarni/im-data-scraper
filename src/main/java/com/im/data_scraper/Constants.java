package com.im.data_scraper;

import java.util.regex.Pattern;

public interface Constants {

	final String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36";
	
	// Pattern constants
	Pattern VIDEO_COMMENT_COUNT_PATTERN = Pattern.compile("commentcount:(\\d+?),", Pattern.CASE_INSENSITIVE);
	Pattern VIDEO_VIEW_COUNT_PATTERN = Pattern.compile("viewcount:\"(\\S+?)\"", Pattern.CASE_INSENSITIVE);
	Pattern VIDEO_SHARE_COUNT_PATTERN = Pattern.compile("sharecount:(\\d+?)," , Pattern.CASE_INSENSITIVE);
	Pattern VIDEO_LIKE_COUNT_PATTERN = Pattern.compile("likecount:(\\d+?)," , Pattern.CASE_INSENSITIVE);
	Pattern VIDEO_DESCRIPTION_PATTERN = Pattern.compile("property\\s*=\\s*\"og:description\"\\s*content\\s*=\\s*\"([\\s\\S]*?)\"", Pattern.CASE_INSENSITIVE);
	Pattern VIDEO_PUBLISHED_DATE_PATTERN = Pattern.compile("<abbr\\s*title=\"[\\s\\S]*?\"\\s*data-utime=\"([\\d]*?)\"", Pattern.CASE_INSENSITIVE);

	//patterns for video discovery
	Pattern LAST_FB_VIDEO_ID_PATTERN = Pattern.compile("\"last_fbid\":([\\d]+?),", Pattern.CASE_INSENSITIVE);
	Pattern FB_PAGE_ID_PATTERN = Pattern.compile("\"page\":([\\d]+?),", Pattern.CASE_INSENSITIVE);
	Pattern FB_VIDEO_PAGINATION_CURSOR_PATTERN = Pattern.compile("\"cursor\":\"([\\s\\S]*?)\"", Pattern.CASE_INSENSITIVE);
	Pattern VIDEO_DISCOVERY_DETAILS_PATTERN = Pattern.compile("href=\"(\\/\\S+?\\/videos\\/\\d+?\\/)\"[\\s\\S]*?duration:([\\s\\S]+?)\"[\\s\\S]*?title=\"([\\s\\S]+?)\"",Pattern.CASE_INSENSITIVE);
	Pattern VIDEO_THUMBNAIL_PATTERN = Pattern.compile("\"scaledImageFitWidth\\s*img\" src=(\"[\\s\\S]*?)\"", Pattern.CASE_INSENSITIVE);
	// AWS constants
	String AWS_ACCESS_KEY = "AKIAIEBGMZ2KDFYTA74Q";
	String AWS_SECRETE_KEY = "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";
	
	String AWS_S3_BUCKET = "cm-crawled-data";
	String AWS_S3_FACEBOOK_BASE_FOLDER = "im/v1/facebook/scraper/";
	String AWS_S3_INSTAGRAM_BASE_FOLDER = "im/v1/instagram/scraper/";
	
	String AWS_S3_INSTGRAM_ACCOUNT_INPUT_FILE = "im/v1/instagram/scraper/input/insta_acc_input.txt";
	String AWS_S3_INSTGRAM_MEDIA_INPUT_FILE = "im/v1/instagram/scraper/input/insta_video_input.txt";
	
	String JOB_TYPE_FB_VIDEO = "FB_VIDEO";
	String JOB_TYPE_FB_PAGE	= "FB_PAGE";
	String JOB_TYPE_INSTA_ACCOUNT = "INSTA_ACCOUNT";
	String JOB_TYPE_INSTA_VIDEO = "INSTA_VIDEO";
	String JOB_TYPE_INSTA_ACCOUNT_FILE = "INSTA_ACCOUNT_FILE";
	String JOB_TYPE_FB_VIDEO_DISCOVERY = "FB_VIDEO_DISCOVERY";
	
	int PAGINATION_COUNT = 20;
}
