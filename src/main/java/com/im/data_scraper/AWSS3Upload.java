package com.im.data_scraper;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

public class AWSS3Upload {
	private static AWSCredentials credentials = null;
	private static AmazonS3 s3Client = null;

	static {
		credentials = new BasicAWSCredentials(Constants.AWS_ACCESS_KEY, Constants.AWS_SECRETE_KEY);
		s3Client = new AmazonS3Client(credentials);
	}

	public static void uploadDataToS3(String jobType, String fileName, String outputFolderPath) {
		// AWSCredentials credentials = new
		// BasicAWSCredentials(Constants.AWS_ACCESS_KEY,
		// Constants.AWS_SECRETE_KEY);
		// AmazonS3 s3Client = new AmazonS3Client(credentials);

		// Check if date folder exists or not
		Date date = new Date();
		String today = new SimpleDateFormat("yyyy-MM-dd").format(date);
		String folderName = today = "date=" + today + "/";

		String childFolder = "page-stats/";
		if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_FB_VIDEO)) {
			childFolder = Constants.AWS_S3_FACEBOOK_BASE_FOLDER + "video-stats/";
		} else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_FB_PAGE)) {
			childFolder = Constants.AWS_S3_FACEBOOK_BASE_FOLDER + "page-stats/";
		} else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_INSTA_ACCOUNT)) {
			childFolder = Constants.AWS_S3_INSTAGRAM_BASE_FOLDER + "account/";
		} else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_INSTA_VIDEO)) {
			childFolder = Constants.AWS_S3_INSTAGRAM_BASE_FOLDER + "media/";
		}else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_INSTA_ACCOUNT_FILE)) {
			childFolder = Constants.AWS_S3_INSTAGRAM_BASE_FOLDER + "account/";
		}else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_FB_VIDEO_DISCOVERY)) {
			childFolder = Constants.AWS_S3_FACEBOOK_BASE_FOLDER + "video-discovery/";
		}

		boolean isExists = exists(s3Client, childFolder, folderName);
		if (!isExists) {
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(0);
			// create empty content
			InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

			PutObjectRequest putObjectRequest = new PutObjectRequest(Constants.AWS_S3_BUCKET, childFolder + folderName,
					emptyContent, metadata);
			s3Client.putObject(putObjectRequest);
		}

		PutObjectRequest putObjectRequest = new PutObjectRequest(Constants.AWS_S3_BUCKET,
				childFolder + folderName + fileName, new File(outputFolderPath + fileName));
		s3Client.putObject(putObjectRequest);

	}

	private static boolean exists(AmazonS3 s3Client, String childFolder, String folderName) {
		try {
			s3Client.getObjectMetadata(Constants.AWS_S3_BUCKET, childFolder + folderName);
		} catch (AmazonServiceException e) {
			return false;
		}
		return true;
	}

	public static void downloadFileFromS3(String jobType, String filePath) {
		try {
			S3Object instagramInput = null;
			if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_INSTA_VIDEO)) {
				instagramInput = s3Client.getObject(
						new GetObjectRequest(Constants.AWS_S3_BUCKET, Constants.AWS_S3_INSTGRAM_MEDIA_INPUT_FILE));
			}else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_INSTA_ACCOUNT)) {
				instagramInput = s3Client.getObject(
						new GetObjectRequest(Constants.AWS_S3_BUCKET, Constants.AWS_S3_INSTGRAM_ACCOUNT_INPUT_FILE));
			}
			writeS3ToDisk(instagramInput.getObjectContent(), filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void writeS3ToDisk(InputStream inputStream, String filePath) throws IOException {
		// Read the text input stream one line at a time and display each line.
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		String inputLine;
		StringBuffer input = new StringBuffer();
		while ((inputLine = reader.readLine()) != null) {
			input.append(inputLine);
			input.append("\n");
		}
		PrintWriter printWriter = new PrintWriter(new FileWriter(filePath));
		printWriter.write(input.toString());
		printWriter.flush();
		printWriter.close();
	}
}
