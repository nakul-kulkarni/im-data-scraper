package com.im.data_scraper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;

import org.apache.commons.lang.IncompleteArgumentException;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

public class App {
	private static int count;
	private static int startCount = 0;
	private static int batchCount;

	private URL urlLink;
	private HttpURLConnection httpURLConnection;

	private String globalfileName;
	private String globalOutputPath;

	public static void main(String[] args) throws Exception {
		App app = new App();

		try {

			String jobType = args[0];
			String filePath = args[1];
			String outputFolderPath = args[2];

			if (args[3] != null) {
				startCount = Integer.parseInt(args[3]);
			} else {
				startCount = 0;
			}

			String fileName = null;

			// String jobType = Constants.JOB_TYPE_FB_VIDEO; // String filePath =
			// "/Users/Nakul/Documents/fb_video_ids.csv";

			// String jobType = Constants.JOB_TYPE_FB_PAGE; // String filePath =
			// "/Users/Nakul/Documents/pageInput.txt";

			// String jobType = Constants.JOB_TYPE_INSTA_VIDEO; // String filePath =
			// "/Users/Nakul/Documents/insta_video_input.txt";

			// String jobType = Constants.JOB_TYPE_INSTA_ACCOUNT;
			// String filePath = "/home/nakul/Documents/insta_acc_input.txt";

			// String jobType = Constants.JOB_TYPE_INSTA_ACCOUNT_FILE; // String filePath =
			// "/Users/Nakul/Documents/2010";

			// String outputFolderPath = "/home/nakul/Documents/";

			if (StringUtils.isEmpty(filePath) && StringUtils.isEmpty(jobType)) {
				System.out.println(
						"Pass the args as : 1) Job type: FB_VIDEO/FB_PAGE/INSTA_ACCOUNT/INSTA_VIDEO/INSTA_ACCOUNT_FILE/FB_VIDEO_DISCOVERY");
				System.out.println("2) input filePath: location to the file e.g. /Users/Nakul/Documents/pageInput.txt");
				System.out.println("3) output folder path: location to generate output e.g. /Users/Nakul/Documents/");
				System.out.println("4) Optional starting point for scraping");
				throw new IncompleteArgumentException("Please provide valid arguments");
			}
			Date date = new Date();
			String today = new SimpleDateFormat("yyyy-MM-dd").format(date);

			if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_FB_VIDEO)) {
				fileName = "video_stats_output_" + today + ".json";
			} else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_FB_PAGE)) {
				fileName = "page_stats_output_" + today + ".json";
			} else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_FB_VIDEO_DISCOVERY)) {
				fileName = "fb_video_disocvery_" + today + ".json";
			} else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_INSTA_VIDEO)) {
				// AWSS3Upload.downloadFileFromS3(jobType, filePath);
				fileName = "instagram_video_stats_output_" + today + ".json";
			} else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_INSTA_ACCOUNT)) {
				// AWSS3Upload.downloadFileFromS3(jobType, filePath);
				fileName = "instagram_account_stats_output_" + today + ".json";
			} else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_INSTA_ACCOUNT_FILE)) {
				fileName = "instagram_account_stats_output_for_existing_folder_" + today + ".json";
			}

			app.globalOutputPath = outputFolderPath;
			app.globalfileName = fileName;
			List<JSONObject> jsonObjects = app.readFile(jobType, filePath);
			if (jsonObjects.size() > 0) {
				boolean isAppend = false;
				App.writeJsonToFile(jsonObjects, fileName, outputFolderPath, isAppend);
				AWSS3Upload.uploadDataToS3(jobType, fileName, outputFolderPath);
			}
			System.out.println("Job completed!!!");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(
					"Pass the args as : 1) Job type: FB_VIDEO/FB_PAGE/INSTA_ACCOUNT/INSTA_VIDEO/INSTA_ACCOUNT_FILE/FB_VIDEO_DISCOVERY");
			System.out.println("2) input filePath: location to the file e.g. /Users/Nakul/Documents/pageInput.txt");
			System.out.println("3) output folder path: location to generate output e.g. /Users/Nakul/Documents/");
			System.out.println("4) Optional starting point for scraping");
			throw new IncompleteArgumentException("Please provide valid arguments");
		}
	}

	private JSONObject getExistingResponse(String fileName) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			StringBuilder stringBuilder = new StringBuilder();
			String line = null;
			String ls = System.getProperty("line.separator");
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}
			// delete the last new line separator
			stringBuilder.deleteCharAt(stringBuilder.length() - 1);
			reader.close();
			String content = stringBuilder.toString();
			JSONObject jsonObject = this.extractInstagramAccountData(null, content);
			return jsonObject;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * readFile
	 * 
	 * @param jobType
	 * @param filePath
	 */
	private List<JSONObject> readFile(String jobType, String filePath) {
		List<JSONObject> jsonObjects = null;
		jsonObjects = new LinkedList<JSONObject>();
		JSONObject jsonObject = null;
		try {
			if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_INSTA_ACCOUNT_FILE)) {
				File folder = new File(filePath);
				File[] listOfFiles = folder.listFiles();
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						String fileName = listOfFiles[i].getAbsolutePath();
						jsonObject = this.getExistingResponse(fileName);
						if (null != jsonObject) {
							jsonObjects.add(jsonObject);
						}
					}
				}
			} else {
				BufferedReader reader;
				reader = new BufferedReader(new FileReader(filePath));
				String line;
				int checkStartPoint = 0;
				while ((line = reader.readLine()) != null) {
					count += 1;
					if (checkStartPoint != startCount) {
						checkStartPoint += 1;
						continue;
					}
					batchCount += 1;

					System.out.println(count);
					if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_FB_VIDEO)) {
						StringBuffer response = this.getURLResponse(line);
						jsonObject = this.extractVideoData(response, line);
					} else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_FB_PAGE)) {
						jsonObject = this.extractPageData(line);
					} else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_INSTA_ACCOUNT)) {
						jsonObject = this.extractInstagramAccountData(line, null);
					} else if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_INSTA_VIDEO)) {
						jsonObject = this.extractInstagramVideoData(line);
					}
					if (null != jsonObject) {
						jsonObjects.add(jsonObject);
					}
					System.out.println("Current time: " + new Date());
					if (batchCount == 100) {
						App.writeJsonToFile(jsonObjects, count + "_" + globalfileName, globalOutputPath, false);
						jsonObjects = new LinkedList<JSONObject>();
						batchCount = 0;
						AWSS3Upload.uploadDataToS3(jobType, count + "_" + globalfileName, globalOutputPath);
						System.out.println("Batch uploaded!");
					}
				}
				reader.close();
			}
			// System.out.println("Current time: " + new Date());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return jsonObjects;
	}

	/**
	 * writeJsonToFile
	 * 
	 * @param jsonObjects
	 * @param fileName
	 * @param outputFolderPath
	 * @throws IOException
	 */
	public static void writeJsonToFile(List<JSONObject> jsonObjects, String fileName, String outputFolderPath,
			boolean isAppend) throws IOException {
		if (null != fileName) {
			PrintWriter printWriter = new PrintWriter(new FileWriter(outputFolderPath + fileName, isAppend));
			for (JSONObject jsonObject : jsonObjects) {
				printWriter.write(jsonObject.toJSONString());
				printWriter.println();
				printWriter.flush();
			}
			printWriter.close();
		}
	}

	/**
	 * extractPageData
	 * 
	 * @param pageURL
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private JSONObject extractPageData(String pageURL) throws IOException {
		JSONObject jsonObject = new JSONObject();
		try {
			String pageId = pageURL.substring(pageURL.lastIndexOf("/") + 1, pageURL.length());
			jsonObject.put("pageId", pageId);
			Document doc = org.jsoup.Jsoup.connect(pageURL).userAgent(
					"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36")
					.header("accept-language", "en-US,en;q=0.9").timeout(10 * 10000).get();
			// For pages:
			Element content = doc.getElementById("PagesLikesCountDOMID");
			if (null != content && content.child(0).childNodes().size() > 0) {
				String pageLikes = content.child(0).childNodes().get(0).toString();
				jsonObject.put("pageLikes", pageLikes.replace(",", "").trim());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	/**
	 * extractVideoData
	 * 
	 * @param response
	 * @param videoURL
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public JSONObject extractVideoData(StringBuffer response, String videoURL) throws IOException {
		String videoResponse = response.toString();
		String videoId = videoURL.substring(videoURL.lastIndexOf("/") + 1, videoURL.length());
		Matcher commentMtch = Constants.VIDEO_COMMENT_COUNT_PATTERN.matcher(videoResponse);
		Matcher viewMtch = Constants.VIDEO_VIEW_COUNT_PATTERN.matcher(videoResponse);
		Matcher shareMtch = Constants.VIDEO_SHARE_COUNT_PATTERN.matcher(videoResponse);
		Matcher likeMtch = Constants.VIDEO_LIKE_COUNT_PATTERN.matcher(videoResponse);
		Matcher descriptionMtch = Constants.VIDEO_DESCRIPTION_PATTERN.matcher(videoResponse);
		Matcher publishedDateMtch = Constants.VIDEO_PUBLISHED_DATE_PATTERN.matcher(videoResponse);

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("videoId", videoId);
			while (commentMtch.find()) {
				jsonObject.put("commentsCount", commentMtch.group(1).replace(",", ""));
			}
			while (viewMtch.find()) {
				jsonObject.put("viewsCount", viewMtch.group(1).replace(",", ""));
			}
			while (shareMtch.find()) {
				jsonObject.put("sharesCount", shareMtch.group(1).replace(",", ""));
			}
			while (likeMtch.find()) {
				jsonObject.put("likesCount", likeMtch.group(1).replace(",", ""));
			}
			while (descriptionMtch.find()) {
				try {
					String description = StringEscapeUtils.unescapeHtml(descriptionMtch.group(1).replace(",", ""));
					jsonObject.put("description", description.replaceAll("\\\\", ""));
				} catch (Exception e) {
					System.out.println(descriptionMtch.group(1).replace(",", ""));
					e.printStackTrace();
				}
			}
			while (publishedDateMtch.find()) {
				Long date = Long.parseLong(publishedDateMtch.group(1).replace(",", ""));
				Date date2 = new Date(date * 1000L);
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
				String formatted = format.format(date2);
				jsonObject.put("publisehdAt", formatted);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	@SuppressWarnings("unchecked")
	private JSONObject extractInstagramVideoData(String line) {
		JSONObject videoData = null;
		try {
			Document doc = org.jsoup.Jsoup.connect(line).timeout(10 * 1000).get();
			Node node = doc.childNode(1).childNode(2).childNode(3).childNode(0);
			String data = node.attr("data").replaceAll("window._sharedData = ", "").replaceAll(";", "");
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(data);
			JSONObject entry = (JSONObject) jsonObject.get("entry_data");
			JSONArray jsonArray = (JSONArray) entry.get("PostPage");
			JSONObject profile = (JSONObject) jsonArray.get(0);
			JSONObject graphql = (JSONObject) profile.get("graphql");
			JSONObject mediaData = (JSONObject) graphql.get("shortcode_media");
			JSONObject commentEdge = (JSONObject) mediaData.get("edge_media_to_comment");
			JSONObject likeEdge = (JSONObject) mediaData.get("edge_media_preview_like");
			JSONObject titleEdge = (JSONObject) mediaData.get("edge_media_to_caption");

			JSONArray edgesArray = (JSONArray) titleEdge.get("edges");
			JSONObject nodeEdge = null;
			JSONObject nodeObject = null;

			if (edgesArray.size() > 0) {
				nodeEdge = (JSONObject) edgesArray.get(0);
				nodeObject = (JSONObject) nodeEdge.get("node");
			}
			JSONObject owner = (JSONObject) mediaData.get("owner");

			videoData = new JSONObject();
			videoData.put("videoId", mediaData.get("id"));
			if (null != nodeObject) {
				videoData.put("title", nodeObject.get("text"));
			} else {
				videoData.put("title", null);
			}
			videoData.put("thumbnail", mediaData.get("display_url"));
			videoData.put("videoCode", mediaData.get("shortcode"));
			videoData.put("isVideo", mediaData.get("is_video"));
			videoData.put("videoViewsCount", mediaData.get("video_view_count"));
			videoData.put("commentsCount", commentEdge.get("count"));
			videoData.put("likesCount", likeEdge.get("count"));
			videoData.put("accountId", owner.get("id"));
			videoData.put("accountName", owner.get("username"));
			videoData.put("publishedAt", mediaData.get("taken_at_timestamp"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return videoData;
	}

	@SuppressWarnings("unchecked")
	private JSONObject extractInstagramAccountData(String accountId, String response) {
		JSONObject accountDetails = null;
		try {
			Document doc = null;
			Node node = null;
			if (null != accountId) {
				doc = Jsoup.connect("http://instagram.com/" + accountId).timeout(10 * 10000).get();
			} else if (null != response) {
				doc = Jsoup.parse(response);
			}
			Elements scriptElements = doc.getElementsByTag("script");
			for (int i = 0; i < scriptElements.size(); i++) {
				Element element = scriptElements.get(i);
				try {
					if (element.childNode(0).attr("data").contains("window._sharedData = ")) {
						node = element.childNode(0);
						break;
					}
				} catch (Exception e) {
					System.out.println("Exception while iterating---" + e);
				}
			}

			String data = node.attr("data").replaceAll("window._sharedData = ", "").replaceAll(";", "");
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(data);
			JSONObject entry = (JSONObject) jsonObject.get("entry_data");
			JSONArray jsonArray = (JSONArray) entry.get("ProfilePage");
			JSONObject profile = (JSONObject) jsonArray.get(0);
			JSONObject graphql = (JSONObject) profile.get("graphql");
			JSONObject user = (JSONObject) graphql.get("user");
			JSONObject mediaEdge = (JSONObject) user.get("edge_owner_to_timeline_media");
			JSONObject followEdge = (JSONObject) user.get("edge_follow");
			JSONObject followedByEdge = (JSONObject) user.get("edge_followed_by");
			JSONArray edges = (JSONArray) mediaEdge.get("edges");

			System.out.println("Size of media: " + edges.size());
			JSONArray videoDataArr = new JSONArray();
			for (int i = 0; i < edges.size(); i++) {
				JSONObject videoData = new JSONObject();
				JSONObject videoLevelData = (JSONObject) edges.get(i);
				JSONObject edgeNode = (JSONObject) videoLevelData.get("node");
				JSONObject likeEdge = (JSONObject) edgeNode.get("edge_liked_by");
				JSONObject titleEdge = (JSONObject) edgeNode.get("edge_media_to_caption");
				JSONArray edgesArray = (JSONArray) titleEdge.get("edges");

				JSONObject nodeEdge = null;
				JSONObject nodeObject = null;
				if (edgesArray.size() > 0) {
					nodeEdge = (JSONObject) edgesArray.get(0);
					nodeObject = (JSONObject) nodeEdge.get("node");
					videoData.put("title", nodeObject.get("text"));
				}

				videoData.put("mediaId", edgeNode.get("id"));
				videoData.put("isVideo", edgeNode.get("is_video"));
				videoData.put("videoCode", edgeNode.get("shortcode"));
				videoData.put("likesCount", likeEdge.get("count"));
				// videoData.put("title", nodeObject.get("text"));
				JSONObject videoComment = (JSONObject) edgeNode.get("edge_media_to_comment");
				videoData.put("commentsCount", videoComment.get("count"));
				videoData.put("publisehdAt", edgeNode.get("taken_at_timestamp"));
				videoData.put("thumbnail", edgeNode.get("thumbnail_src"));
				videoData.put("viewsCount", edgeNode.get("video_view_count"));

				videoDataArr.add(videoData);
			}

			accountDetails = new JSONObject();
			accountDetails.put("accountId", user.get("id"));
			accountDetails.put("userName", user.get("username"));
			accountDetails.put("fullName", user.get("full_name"));
			accountDetails.put("profilePicture", user.get("profile_pic_url"));
			accountDetails.put("mediaCount", mediaEdge.get("count"));
			accountDetails.put("followsCount", followEdge.get("count"));
			accountDetails.put("followedByCount", followedByEdge.get("count"));
			accountDetails.put("mediaIds", videoDataArr);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return accountDetails;
	}

	/**
	 * getURLResponse
	 * 
	 * @param videoURL
	 * @return StringBuffer
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public StringBuffer getURLResponse(String videoURL) throws InterruptedException {
		StringBuffer response = null;
		BufferedReader bufferedReader = null;
		try {
			urlLink = new URL("https://facebook.com/" + videoURL);
			httpURLConnection = (HttpURLConnection) urlLink.openConnection();
			// add request header
			httpURLConnection.setRequestProperty("User-Agent", Constants.USER_AGENT);
			httpURLConnection.setRequestProperty("accept-language", "en-US,en;q=0.9");
			httpURLConnection.setConnectTimeout(60000);
			try {
				int responseCode = httpURLConnection.getResponseCode();
				System.out.println("Response Code : " + responseCode);
				bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
			} catch (UnknownHostException e) {
				System.out.println("Hmm...Might be an internet issue which is too fast... Ha Ha");
				System.out.println("Let's retry this again after sometime");
				TimeUnit.SECONDS.sleep(3);
				// Try one more time if it fails
				bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
			}
			String inputLine;
			response = new StringBuffer();
			int countTocheckStuck = 0;
			while ((inputLine = bufferedReader.readLine()) != null) {
				if (countTocheckStuck >= 100) {
					break;
				}
				countTocheckStuck++;
				response.append(inputLine);
			}
			bufferedReader.close();
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

}
