package com.im.data_scraper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class VideoDiscovery {

	private List<JSONObject> jsonObjects = null;
	private static int paginationCount;
	private static int count;
	private static int startCount = 0;
	//private List<String> videoIdInput = null;

	private String globalfileName;
	private String globalOutputPath;
	Set<String> uniqueVideoId = null;

	public static void main(String[] args) throws IOException {

		String fileName = null;
		String jobType = args[0];
		String filePath = args[1];
		String outputFolderPath = args[2];
		if (args[3] != null) {
			startCount = Integer.parseInt(args[3]);
		}else {
			startCount = 0;
		}

		/*String jobType = Constants.JOB_TYPE_FB_VIDEO_DISCOVERY;
		String filePath = "/home/nakul/CM-Work/Scraper-Input/adhoc_indonesia.txt";
		String outputFolderPath = "/home/nakul/CM-Work/Scraper-Input/output/";*/
		
		VideoDiscovery discovery = new VideoDiscovery();

		Date date = new Date();
		String today = new SimpleDateFormat("yyyy-MM-dd").format(date);

		if (jobType.equalsIgnoreCase(Constants.JOB_TYPE_FB_VIDEO_DISCOVERY)) {
			fileName = "discovered_video_stats_output_" + today + ".json";
		}
		discovery.globalfileName = fileName;
		discovery.globalOutputPath = outputFolderPath;
		discovery.getVideoDiscoveryFirstPageResponse(filePath);
		System.out.println("***********Job completed***********");
	}

	@SuppressWarnings("unchecked")
	private void getVideoDiscoveryFirstPageResponse(String filePath) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filePath));
			String line = null;
			List<JSONObject> videoStatsDataList =  new ArrayList<JSONObject>();
			int checkStartPoint = 0;
			while ((line = reader.readLine()) != null) {
				count += 1;
				System.out.println("Video Discovery for Page number:"+ count);
				if (checkStartPoint != startCount) {
					checkStartPoint += 1;
					continue;
				}
				paginationCount = 0;
				jsonObjects = new LinkedList<JSONObject>();
				//videoIdInput = new ArrayList<String>();
				uniqueVideoId = new HashSet<String>();
				
				String pageId = line.substring(line.lastIndexOf("/") + 1, line.length());
				System.out.println("Video Discovery for PageId:"+ pageId);
				Document doc = org.jsoup.Jsoup
						.connect(
								line + "/videos/?ref=page_internal&fb_dtsg_ag=AQwMWnCBhmK4axgYpoQHEPfYxKFrgDAV99rYVO8W9kyrBw%3AAQzuCS_o8GKds66Gims8ZUvReF2dbXgJCqDxOuQsguKKPQ&ajaxpipe=1&ajaxpipe_token=AXgm5inxJWmAePka&path=%2Fcocacolaindia%2Fvideos%2F&__user=1424487532&__a=1&__dyn=7AgNe-4amaUmgDxiWJGi9FxqeCwDKEyGgS8UR9LFGUqx-6ES2N6xCay8KFGUpxSaxuq32bG4UJoK48G5WAxamjDK7Hze3KFU9EggHzobp94rzLxe48pDAyF8O49E6y5bBy8G6Ehwj8lg8-i49842E-dzVK5e9wg8lDCzopwxzoGqfw-KEK4ooAghzRGm5Apy8lwxgC3mbKbzUC26dwjUgUkBzXHzEy3C4oK4u9Ay8iAUGvww-UlzAjxfyopxmcmECmUhDzEjVEa89ofoqwiAaKezHAy8uyUaoGWyXwPxecDy9Upy8hGq17CUO5AbxS226poa8&__req=fetchstream_2&__be=1&__pc=PHASED%3Aufi_home_page_pkg&dpr=1&__rev=4675119&jazoest=28177&__spin_r=4675119&__spin_b=trunk&__spin_t=1547121531&__adt=2&ajaxpipe_fetch_stream=1?")
						.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36")
						.header("accept-language", "en-US").header("accept", "text/html, application/xhtml+xml, */*")
						.header("Accept-Encoding", "gzip, deflate").ignoreContentType(true).timeout(10 * 10000).get();

				this.getNextPageForVideo(doc, pageId);
				for(JSONObject jsonObject:jsonObjects){
					String videoId = jsonObject.get("videoId").toString();	
					String videoUrl = pageId+"/videos/"+videoId;
					JSONObject videoData = this.getVideoStatsData(videoUrl);
					
					videoData.put("pageId", pageId);
					videoData.put("videoId", jsonObject.get("videoId"));
					videoData.put("videoDuration", jsonObject.get("videoDuration"));
					videoData.put("videoTitle", jsonObject.get("videoTitle"));
					videoData.put("videoThumbnail", jsonObject.get("videoThumbnail"));
					videoStatsDataList.add(videoData);
				}
				Boolean isAppend = false;
				if (videoStatsDataList.size() > 0) {
					App.writeJsonToFile(videoStatsDataList,  pageId + "_" + globalfileName, globalOutputPath,isAppend);
					videoStatsDataList.clear();
					System.out.println("Video Discovery data added to output file for pageID: "+ pageId);
					System.out.println("Current time: " + new Date());
					AWSS3Upload.uploadDataToS3(Constants.JOB_TYPE_FB_VIDEO_DISCOVERY, pageId + "_" + globalfileName, globalOutputPath);
					System.out.println("File uploaded to s3 for pageID : "+ pageId);
				}
			}
			
			reader.close();
			/*PrintWriter printWriter = new PrintWriter(
					new FileWriter("/Users/Nakul/Documents/paginated-discovery/" + pageId + ".json"));
			for (JSONObject jsonObject : jsonObjects) {
				printWriter.write(jsonObject.toJSONString());
				printWriter.println();
				printWriter.flush();
			}
			printWriter.close();
			
			PrintWriter videoStatsInputWriter = new PrintWriter(
					new FileWriter("/Users/Nakul/Documents/paginated-discovery/video-stats-input/" + pageId + "txt"));
			for (String videoStatsInput : videoIdInput) {
				videoStatsInputWriter.write(videoStatsInput);
				videoStatsInputWriter.println();
				videoStatsInputWriter.flush();
			}
			videoStatsInputWriter.close();*/
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	private JSONObject getVideoStatsData(String videoUrl) {
		App app = new App();
		try {
			StringBuffer response = app.getURLResponse(videoUrl);
			System.out.println("Getting VideoStats Data : "+ count + "--------"+ new Date());
			JSONObject videoStatsData = app.extractVideoData(response, videoUrl);
			if (videoStatsData != null) {
				return videoStatsData;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public void extractVideoDiscoveryData(Document doc, String pageId) {
		String videoId = null;
		String videoDuration = null;
		String videoTitle = null;
		String videoThumbnail = null;

		String data = doc.toString();
		data = data.replaceAll("\\\\", "");
		Matcher videoMatch = Constants.VIDEO_DISCOVERY_DETAILS_PATTERN.matcher(data);
		JSONObject videoData = null;
		while (videoMatch.find()) {
			videoId = videoMatch.group(1);
			videoId = videoId.substring(0, videoId.length() - 1);
			if (uniqueVideoId.contains(videoId)) {
				continue;
			}
			uniqueVideoId.add(videoId);
			videoData = new JSONObject();
			videoData.put("pageId", pageId);
			videoId = videoId.substring(videoId.lastIndexOf("/") + 1, videoId.length());
			videoData.put("videoId", videoId);
			videoDuration = videoMatch.group(2).
					replaceAll("hours", "H").
					replaceAll("minutes", "M").
					replaceAll("seconds", "S").
					replaceAll("hour", "H").
					replaceAll("minute", "M").
					replaceAll("second", "S").
					replaceAll(" ", "");
			videoData.put("videoDuration", videoDuration);
			videoTitle = StringEscapeUtils.unescapeHtml(videoMatch.group(3));
			videoData.put("videoTitle", videoTitle);
			
			Matcher thumbnailMatch = Constants.VIDEO_THUMBNAIL_PATTERN.matcher(data);
			while (thumbnailMatch.find()) {
				videoThumbnail = StringEscapeUtils.unescapeHtml(thumbnailMatch.group(1).replace("\"", ""));
				videoData.put("videoThumbnail", videoThumbnail);
			}
			jsonObjects.add(videoData);
			//String videoStatsInput = pageId + "/videos/" + videoId;
			//videoIdInput.add(videoStatsInput);
		}
	}

	private void getNextPageForVideo(Document doc, String pageId) {
		try {
			this.extractVideoDiscoveryData(doc, pageId);
			// paginate for first 10 pages only.
			if (paginationCount != Constants.PAGINATION_COUNT) {
				String data = doc.toString();
				data = data.replaceAll("\\\\", "");

				String cursor = null;
				String paginationId = null;
				String lastFbId = null;

				Matcher lastFbIdMatch = Constants.LAST_FB_VIDEO_ID_PATTERN.matcher(data);
				while (lastFbIdMatch.find()) {
					lastFbId = lastFbIdMatch.group(1);
				}

				Matcher pageMatch = Constants.FB_PAGE_ID_PATTERN.matcher(data);
				while (pageMatch.find()) {
					paginationId = pageMatch.group(1);
				}

				Matcher paginationMatch = Constants.FB_VIDEO_PAGINATION_CURSOR_PATTERN.matcher(data);

				while (paginationMatch.find()) {
					cursor = paginationMatch.group(1);
				}

				if (cursor != null) {
					System.out.println("Getting Paginated Data: " + cursor);
					paginationCount += 1;
					Document nextPageDoc = Jsoup
							.connect(
									"https://www.facebook.com/ajax/pagelet/generic.php/PagesVideoHubVideoContainerPagelet?dpr=1&data=%7B%22last_fbid%22%3A"
											+ lastFbId + "%2C%22page%22%3A" + paginationId
											+ "%2C%22playlist%22%3Anull%2C%22cursor%22%3A%22" + cursor
											+ "%22%7D&__user=0&__a=1&__dyn=5V8WXBzamaUmgDBzFHpUR1ycCzSczVbGAdyeGBXrWqF1eU8EnGdwIhEnUF7yWCHAxiESmqaxuqE88HyWxeipi28gyElWAAzppenKtqx2AcUhz998iGtxifGcgLAKidzoKnGh4-9AZ4gO445ECiajz8gzAaUx5G3CmaBKVojDBDAyVpXgS2quElHx-q9CJ4g89EiGfVbmiaCUkV-axyHu4oth4dz8OiquUCdyFE-2h6x6WLGHzui4p5UBaBKhWADBCEyS8DDio8lfwMAyXyWgkoKmQFKbUC2C6ogUK8BzUyuGrLAzEC54WzEKiehVkEGaVeaDCxZai9CBAJ1e4EKdBAABVp4ehbx6uegkCyEy9DADAyEOESbx-GDz8x12V98gjCx2VbzpHAgC78KcBx6i9AQbKbU9aw&__req=v&__be=-1&__pc=PHASED%3ADEFAULT&__rev=4128790")
							.userAgent(
									"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36")
							.header("accept-language", "en-US,en;q=0.9").ignoreContentType(true).timeout(10 * 10000)
							.get();
					getNextPageForVideo(nextPageDoc, pageId);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
